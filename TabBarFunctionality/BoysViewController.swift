//
//  BoysViewController.swift
//  TabBarFunctionality
//
//  Created by Applied Informatics on 23/03/22.
//

import UIKit

class BoysViewController: UIViewController {
    let boysArray:[String] = ["Faisal", "Junaid", "Aamir", "Moyeen"]
    @IBOutlet weak var boysTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        boysTableView.tableFooterView = UIView()

        // Do any additional setup after loading the view.
    }

}

extension BoysViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return boysArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let bCell = boysTableView.dequeueReusableCell(withIdentifier: "boysCell", for: indexPath)
        bCell.textLabel?.text = boysArray[indexPath.row]
        bCell.selectionStyle = .none
        return bCell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainSB = UIStoryboard(name: "Main", bundle: nil)
        let destSB = mainSB.instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
        destSB.pTitle = boysArray[indexPath.row]
        self.navigationController?.pushViewController(destSB, animated: true)
    }
    
    
}
