//
//  DetailsViewController.swift
//  TabBarFunctionality
//
//  Created by Applied Informatics on 23/03/22.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var details: UITextView!
    @IBOutlet weak var person: UILabel!
    
    var pTitle: String = ""
    var pBody: String = "Hello there. Welcome to the Screen"
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        person.text = pTitle
        details.text = pBody
        
    }

}
