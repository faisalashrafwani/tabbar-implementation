//
//  GirlsViewController.swift
//  TabBarFunctionality
//
//  Created by Applied Informatics on 23/03/22.
//

import UIKit

class GirlsViewController: UIViewController {
    let girlsArray:[String] = ["Alexa", "Shannon", "Shweta", "Anna", "Julie"]
    @IBOutlet weak var girlsTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
extension GirlsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return girlsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let gCell = girlsTableView.dequeueReusableCell(withIdentifier: "girlsCell", for: indexPath)
        gCell.selectionStyle = .none
        gCell.textLabel?.text = girlsArray[indexPath.row]
        return gCell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainSB = UIStoryboard(name: "Main", bundle: nil)
        let destSB = mainSB.instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
        destSB.pTitle = girlsArray[indexPath.row]
        self.navigationController?.pushViewController(destSB, animated: true)
    }
    
    
}
